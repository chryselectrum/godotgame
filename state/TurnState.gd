"""
Container for objects (pawns) taking turns. Also keeps track of turn order.
"""
extends Node

var player_instance_id: int
var player: Actor
var pawns: Dictionary = {} # Pawn
var turn_stack: Array = [] # Models:TurnItem

signal take_action

func _init():
	pass


func register_player(pawn: Pawn):
	player = pawn
	player_instance_id = pawn.get_instance_id()
	_add_pawn(pawn)


func register_pawn(pawn: Pawn):
	_add_pawn(pawn)
	connect("take_action", pawn, "_on_take_action")


func _add_pawn(pawn: Pawn):
	pawns[pawn.get_instance_id()] = pawn


func _on_pawn_acts(pawn: Pawn):
	_add_turn_item(pawn)


func _on_player_acted(player_target):
	_add_turn_item(player)
	emit_signal("take_action", player_target)


func _add_turn_item(pawn: Pawn):
	var turn_item = _create_turn_item(pawn)
	if turn_item:
		turn_stack.append(turn_item)


func _create_turn_item(pawn: Pawn):
	return Models.TurnItem.create_from_pawn(pawn)


func _on_player_acted_finished():
	# sort actions based on their time length
	turn_stack.sort_custom(Models.TurnItem, "sort_by_action_time")

	# act out all the pending pawn (or player) actions in the order of their time length
	while turn_stack.size() > 0:
		if turn_stack[0].action_time > Time.TURN_LENGTH:
			# keep rest of the items in stack and for the next turn
			for item in turn_stack:
				var playerOrPawn: Pawn = pawns[item.instance_id]
				playerOrPawn.act(item, true)
				item.action_time -= Time.TURN_LENGTH

				if playerOrPawn is Actor:
					playerOrPawn.continue_time = true

			return

		var item = turn_stack.pop_front()
		var playerOrPawn = pawns[item.instance_id]

		playerOrPawn.act(item)

		if playerOrPawn is Actor && playerOrPawn.continue_time:
			playerOrPawn.continue_time = false
