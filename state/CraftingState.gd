extends Node

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _on_item_selected(index, items: Array):
	_activated_or_selected(false, index, items)


func _on_item_activated(index, items: Array):
	_activated_or_selected(true, index, items)


func _activated_or_selected(activated, index, items: Array):
	var item = items[index]
	var activated_str = "activated" if activated else "selected"
	print("%s: %s" % [activated_str, item.get_class()])