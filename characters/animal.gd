extends Pawn

class_name Animal
func get_class() -> String: return "Animal"
func is_class(name: String) -> bool: return name == "Animal" or .is_class(name)

signal path_updated

func _ready():
	can_move = true
	state = Enums.MODE.HUNT


func _on_take_action(player_target):
	if !._on_take_action(player_target):
		return

	var current_pos = position
	var current_state = state
	_set_debug_state()
	match current_state:
		Enums.MODE.HUNT:
			if is_zero_goal_position():
				var pos = grid.request_food(current_pos)
				goal_position = pos
				set_current_path(current_pos, goal_position)
			elif turn_state_counter >= 20:
				self.state = Enums.MODE.ROAM
				set_zero_goal_position()
		Enums.MODE.ROAM:
			if is_zero_goal_position() || current_path.size() == 1:
				var pos = grid.request_random(position, 7)
				goal_position = pos
				set_current_path(current_pos, pos)
			elif turn_state_counter >= 40:
				self.state = Enums.MODE.HUNT
				set_zero_goal_position()


	var direction = get_current_path_direction(current_pos)
	var target = grid.request_target(self, direction)

	if grid.is_target_movable(target):
		set_action("move", [target.pos], movement_speed)
	elif grid.is_target_pawn(target):
		set_action("attack", [target.pawn], attack_speed)
		#set_action("flee", [target.pawn], movement_speed)
	else:
		set_action("bump", [])
		self.state = Enums.MODE.ROAM

	action_direction = direction

	TurnState._on_pawn_acts(self)
	#emit_signal("pawn_acts")


func _set_debug_state():
	if Constant.is_debug() && !Constant.remove_debug_state():
		_set_label_text("state: %s, turncount: %d" % [Enums.MODE.keys()[state], turn_state_counter])