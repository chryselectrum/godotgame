extends Animal

class_name Snake
func get_class() -> String: return "Snake"
func is_class(name: String) -> bool: return name == "Snake" or .is_class(name)