extends Pawn

class_name Robot
func get_class() -> String: return "Robot"
func is_class(name: String) -> bool: return name == "Robot" or .is_class(name)