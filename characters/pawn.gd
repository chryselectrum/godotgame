extends KinematicBody2D

class_name Pawn
func get_class() -> String: return "Pawn"
func is_class(name: String) -> bool: return name == "Pawn" or .is_class(name)

export onready var grid: TileMap = get_parent() # TileGrid
export onready var label: Label = Ext.node(self, "Label")
export onready var sprite: Sprite = Ext.node(self, "Sprite")
export onready var anger_sprite: Sprite = Ext.node(self, "Anger")
export onready var animation_player = Ext.node(self, "AnimationPlayer")
export onready var state = Enums.MODE.IDLE setget _set_state

export onready var movement_speed: int
export onready var attack_speed: int
export onready var dig_speed: int


export var start_position = Vector2(0, 0)
export var current_path: Array # PoolVector2Array
export var goal_position = Vector2(0, 0)
export var action_name: String
export var can_move: bool = false
export var turn_state_counter = 0

var global_turn_state_counter = 0
var action_callback
var action_direction: Vector2
var action_args: Array
var action_time: int
var is_first_action: bool = true

#signal pawn_acts
signal pawn_moved

func _init():
	movement_speed = Time.BASE_MOVEMENT_SPEED
	attack_speed = Time.BASE_ATTACK_SPEED
	dig_speed = Time.BASE_DIG_SPEED


func _ready():
	Ext.hide(anger_sprite)
	Ext.center(sprite)
#	sprite.region_enabled = true
#	sprite.region_rect.size.x = Constant.RECT_SIZE
#	sprite.region_rect.size.y = Constant.RECT_SIZE
	#connect("pawn_acts", TurnState, "_on_pawn_acts", [self])
	#connect("pawn_moved", TileNavigation, "_on_pawn_moved")


func _set_state(value):
	if state != value:
		turn_state_counter = 0
	state = value


"""
Tentative (planned) action, virtual
"""
func _on_take_action(player_target):
	var is_ongoing_action = action_name != "" && !is_first_action
	return !is_ongoing_action


"""
Actual action rechecking changes after current turn order actions.
"""
func act(action_item, is_anim_only = false):
	_clear_debug_action()

	var target = grid.request_target(self, action_direction, false)

	if action_name == "move" && !grid.is_target_movable(target):
		set_current_path(position, goal_position)
		bump("bumped into something!")

	elif action_name == "attack" && target.pawn == null:
		bump("my enemy vanished!")

	elif action_name == "attack" && target.pawn != null && self.get_class() == target.pawn.get_class():
		set_current_path(position, goal_position)
		if target.pawn.get_instance_id() == self.get_instance_id():
			bump("bumping into myself?!")
		else:
			bump("friendly!: %s vs %s" % [self.get_class(), target.pawn.get_class()])

	else:
		if is_anim_only && !action_name.ends_with("_anim"):
			action_name += "_anim"

		var local_action_name = action_item.action_name if !is_anim_only else action_name

		if action_item.action_name.begins_with("attack") && anger_sprite:
			anger_sprite.visible = true
		elif anger_sprite:
			anger_sprite.visible = false

		if action_item.action_name.begins_with("attack") && self.get_class() != "Actor":
			print("%s attacks %s!" % [self.get_class(), target.pawn.get_class()])
		_output_debug_target(local_action_name, action_item.action_time, action_args)

		self.callv(local_action_name, action_item.action_args)

		if is_anim_only: return

	action_name = ""
	is_first_action = false
	turn_state_counter += 1
	global_turn_state_counter += 1


func is_zero_goal_position():
	return goal_position.x == 0 && goal_position.y == 0


func set_zero_goal_position():
	goal_position = Vector2(0,0)


func set_action(action_name, args, time = 1):
#	self.action_callback = funcref(self, action_name)
	self.action_name = action_name
	self.action_args = args
	self.action_time = time


func get_current_path_direction(current_pos: Vector2):
	_add_debug_path(current_path)

	var next_pos = Vector2(0, 0)
	if current_path.size() > 1:
		next_pos = current_path.pop_front()
	elif current_path.size() > 0:
		next_pos = current_path.front()

	var direction = (next_pos - current_pos).normalized()
	return direction


func set_current_path(current_pos, pos, distance=10):
	goal_position = pos
	current_path = []
	current_path = Array(grid.nav.get_simple_path(current_pos, pos))
	if current_path.size() > 1:
		current_path.pop_front() # remove current position from the path


func attack(object_pawn):
	_add_debug_action("attack!")
	attack_anim()


func attack_anim():
	set_process(false)
	if animation_player:
		animation_player.play("attack")
		yield(animation_player, "animation_finished")
	set_process(true)


func move(pos):
	set_process(false)
	animation_player.play("walk")

	# Move the node to the target cell instantly,
	# and animate the sprite moving from the start to the target cell
	#var move_direction = (target_position - position).normalized()
	#$Tween.interpolate_property($Pivot, "position", - move_direction * 32, Vector2(), $AnimationPlayer.current_animation_length, Tween.TRANS_LINEAR, Tween.EASE_IN)
	#position = target_position
	#$Tween.start()
	position = pos

	# Stop the function execution until the animation finished
	yield(animation_player, "animation_finished")
	yield(get_tree(), "idle_frame")
	set_process(true)
	#emit_signal("pawn_moved")
	publish_location()


func bump(text = null):
	if text == null:
		_add_debug_action("bump!")
	else:
		_add_debug_action(text)
	bump_anim()


func bump_anim():
	set_process(false)
	if animation_player:
		animation_player.play("bump")
		yield(animation_player, "animation_finished")
	set_process(true)


func _on_publish_location():
	 publish_location()


func publish_location():
	grid.nav.set_blocking_location(get_instance_id(), position)


func publish_initial_location():
	publish_location()


func _set_label_text(value):
	if Constant.is_debug() && label:
		label.text = value


func _add_debug_path(path):
	if Constant.is_debug() && !Constant.remove_debug_paths():
		grid.nav.paths.append(path)


func _add_debug_action(value):
	if Constant.is_debug() && !Constant.remove_debug_actions():
		_set_label_text(value)


func _clear_debug_action():
	if Constant.is_debug() && Constant.remove_debug_state():
		_set_label_text("")


func _output_debug_target(local_action_name, action_time, action_args):
	if !Constant.remove_debug_targets_output():
		print("%s %s:%s %s" % [self.get_class(), local_action_name, action_time, action_args])