extends Camera2D

func _ready() -> void:
	var left = -Constant.BASE_WIDTH / 2
	var top = -((Constant.BASE_HEIGHT / 2) + 16)
	offset.x = left
	offset.y = top
