extends Navigation2D

class_name TileNavigation

export onready var map: TileGrid = $TileGrid # TileMap
export onready var line: Line2D = $Line2D
export onready var paths: Array = []
export onready var lines: Array = []

onready var player: Actor = $TileGrid/Actor # for testing only
onready var half_cell_size = map.cell_size / 2
onready var used_rect: Rect2
onready var used_cells: PoolVector2Array
onready var walkable_ids: Dictionary = {}

var blocked_locations: Dictionary = {}
var navigation_path: Array = []
var a_star: AStar

func _init():
	a_star = AStar.new()


func _ready():
	used_rect = map.get_used_rect()
	used_cells = map.get_used_cells()

	for cell in used_cells:
		var cell_index = map.get_cellv(cell)
		var cell_type = TileGrid.map_from_cell_index_to_cell_type(cell_index)
		# only add and connect walkable points
		if cell_type == Enums.CELL_TYPE.WALKABLE:
			var id = _get_map_id(cell)
			a_star.add_point(id, Vector3(cell.x, cell.y, 0))
			walkable_ids[cell] = id

	for cell in walkable_ids:
		var id = _get_map_id(cell)
		# search around the cell (range(3) returns 0, 1, and 2)
		for x in range(3):
			for y in range(3):
				# determine target, convert range variable to -1, 0, and 1
				var target = cell + Vector2(x - 1, y - 1)
				var target_id = _get_map_id(target)
				# do not connect if point is same or point does not exist on a_star
				if cell == target or not a_star.has_point(target_id):
					continue
				a_star.connect_points(id, target_id, true)

	var pawns = map.get_pawns()
	for pawn in pawns:
#		if pawn:
		pawn.publish_initial_location()

	for structure in map.get_structures():
		set_blocking_location(structure.get_instance_id(), structure.position)


"""
Override Navigation2D get_simple_path() with AStar implementation.
"""
func get_simple_path(start, end, optimize=false):
	var start_id = _transform_world_to_map(start)
	var end_id = _transform_world_to_map(end)

	var path_map = a_star.get_point_path(start_id, end_id)
	var path_world = []
	path_world.resize(path_map.size())

	var count = 0
	for point in path_map:
		var point_world = map.map_to_world(Vector2(point.x, point.y)) + half_cell_size
		path_world[count] = point_world
		count += 1

	return path_world


"""
Set temporary pawn (or other object) location to block navigation.
"""
func set_blocking_location(instance_id, pos):
	var map_id = _transform_world_to_map(pos)
	if blocked_locations.has(instance_id):
		var old_map_id = blocked_locations[instance_id]
		a_star.set_point_disabled(old_map_id, false) # enable previous point
	blocked_locations[instance_id] = map_id
	a_star.set_point_disabled(map_id) # disable current point


func _transform_world_to_map(world_pos):
	var map_pos = map.world_to_map(world_pos)
	return _get_closest_id_for_point(map_pos)


func _get_closest_id_for_point(cell, z=0):
	var id = _get_map_id(cell)
	if a_star.has_point(id):
		return id

	return a_star.get_closest_point(Vector3(cell.x, cell.y, z))


func _set_bounds(cell):
	cell.x = min(used_rect.size.x, cell.x)
	cell.y = min(used_rect.size.y, cell.y)


func _unhandled_input(event: InputEvent) -> void:
	if !event.is_action_pressed("click"):
		return
	# for testing only
	_update_navigation_path(player.position - (position - map.position), get_local_mouse_position())


func _update_navigation_path(start_position, end_position):
	# get_simple_path is part of the Navigation2D class
	# it returns a PoolVector2Array of points that lead you from the
	# start_position to the end_position
	navigation_path = get_simple_path(start_position, end_position, true)

	if Constant.is_debug():
		line.points = navigation_path
		line.show()

	# The first point is always the start_position
	navigation_path.remove(0)

	set_process(true)


func clear_lines():
	for line in lines:
		remove_child(line)
	paths.clear()
	lines.clear()


func draw_lines():
	for path in paths:
		var newLine = Line2D.new()
		newLine.points = path
		newLine.show()
		lines.append(newLine)
		add_child(newLine)


func _on_path_updated(path):
	if Constant.is_debug():
		line.points = path
		line.show()


func _physics_process(delta: float) -> void:
	var walk_distance = player.movement_speed * delta
	move_along_path(player, navigation_path, walk_distance)


func move_along_path(pawn, path: Array, distance = 1.0):
	if path.size() == 0: return

	var last_point = pawn.position
	while path.size():
		var distance_between_points = last_point.distance_to(path[0])

		# the position to move to falls between two points
		if distance <= distance_between_points:
			pawn.position = last_point.linear_interpolate(path[0], distance / distance_between_points)
			return

		# the position is past the end of the segment
		distance -= distance_between_points
		last_point = path[0]
		path.remove(0)

	# the character reached the end of the path
	pawn.position = last_point
	set_process(false)


func _get_map_id(point):
	# offset position of tile with the bounds of the tilemap,
	# preventing IDs of less than 0 when points are behind (0, 0)
	var x = point.x - used_rect.position.x
	var y = point.y - used_rect.position.y
	# return the unique ID for the point on the map
	return x + y * used_rect.size.x