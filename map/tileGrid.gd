extends TileMap

class_name TileGrid

var foodPos = []
var waterPos = []

export onready var nav: Navigation2D = get_parent() # TileNavigation

func _init():
	cell_size.x = Constant.RECT_SIZE
	cell_size.y = Constant.RECT_SIZE
	cell_quadrant_size = Constant.RECT_SIZE / 4


func _ready():
	for node in get_children():
		if node is Pawn:
			TurnState.register_pawn(node)

	for rock in get_used_cells_by_id(Resources.TILE.ROCKS):
		foodPos.append(rock)


func request_food(location):
	return _closestLocation(location, foodPos)


func request_water(location):
	return _closestLocation(location, waterPos)


func request_random(location, radius):
	randomize()
	var plusMinusGainX = -1 if randf() > 0.5 else 1
	var plusMinusGainY = -1 if randf() > 0.5 else 1

	var clipped_direction = Vector2(
		plusMinusGainX * round(radius*randf()),
		plusMinusGainY * round(radius*randf()))
	var cell_start = world_to_map(location)
	var cell_target = cell_start + clipped_direction
	return map_to_world(cell_target)


func request_target(pawn, direction, is_initial = true):
	var normalized_direction = direction.normalized()
	var clipped_direction = Vector2(round(normalized_direction.x), round(normalized_direction.y))
	var cell_start = world_to_map(pawn.position)
	var cell_target = cell_start + clipped_direction

	var cell_target_index = get_cellv(cell_target)
	var cell_target_type = map_from_cell_index_to_cell_type(cell_target_index)

	_debug_request_target(pawn, cell_target_type, is_initial)

	match cell_target_type:
		Enums.CELL_TYPE.EMPTY:
			return _get_empty_pawn(cell_target)

		Enums.CELL_TYPE.OBSTACLE:
			return _get_obstacle(cell_target, cell_target_type)

		Enums.CELL_TYPE.OBJECT:
			return _get_object(cell_target, cell_target_type)

		Enums.CELL_TYPE.WALKABLE:
			var object_pawn = _get_cell_pawn(cell_target)
			if object_pawn:
				if object_pawn is Actor || object_pawn is Pawn:
					return _get_actor_pawn(object_pawn, cell_target)
				elif object_pawn is Structure:
					return _get_object(cell_target, Enums.CELL_TYPE.OBJECT)
			return _get_empty_pawn(cell_target)

		Enums.CELL_TYPE.ACTOR:
			var object_pawn = _get_cell_pawn(cell_target)
			if object_pawn && object_pawn.type == Enums.CELL_TYPE.ACTOR:
				return _get_actor_pawn(object_pawn, cell_target)

	return _get_empty_pawn(cell_target)


func get_pawns():
	return _get_of_type(Pawn)


func get_structures():
	return _get_of_type(Structure)


func _get_of_type(type):
	var items = []
	items.resize(get_child_count())
	var count = 0
	for node in get_children():
		if node is type:
			items[count] = node
			count += 1
	items.resize(count)
	return items


func _get_cell_pawn(coordinates):
	for node in get_children():
		var pos = Vector2(-1,-1)

		if node is Pawn:
			pos = node.position
		elif node is Structure:
			pos = node.position

		if world_to_map(pos) == coordinates:
			return node

func _get_cell_resource(coordinates):
	for node in get_children():
		var pos = Vector2(-1,-1)

		if node is ResourceData:
			pos = node.position

		if pos.x != -1 && pos.y != -1 && world_to_map(pos) == coordinates:
			return node
	return null


func _get_actor_pawn(object_pawn, cell_target):
	return _get_grid_item(cell_target, object_pawn)


func _get_empty_pawn(cell_target):
	return _get_grid_item(map_to_world(cell_target) + cell_size / 2)


func _get_obstacle(cell_target, cell_type):
	return _get_grid_item(cell_target, null, cell_type)


func _get_object(cell_target, cell_type):
	return _get_grid_item(cell_target, null, null, cell_type)


func _get_grid_item(pos, pawn = null, obstacle = null, object = null):
	return Models.GridItem.new(pos, pawn, obstacle, object)


func _closestLocation(startPosition: Vector2, objectPos: Array):
	if objectPos.size() == 0:
		return startPosition

	var minDistance = Constant.INT_MAX
	var closestLocation = null
	for pos in objectPos:
		var worldPos = map_to_world(pos)
		var distance = abs(startPosition.distance_to(worldPos))
		if distance < minDistance:
			minDistance = distance
			closestLocation = worldPos

	return closestLocation if closestLocation != null else objectPos.back()


static func is_target_movable(target: Models.GridItem):
	return (target
		&& target.pos
		&& !target.pawn
		&& !target.obstacle
		&& !target.object)


static func is_target_pawn(target: Models.GridItem):
	return (target
		&& target.pos
		&& target.pawn)


func is_target_diggable(target: Models.GridItem, resource_object_out: Array):
	var is_diggable = target && (target.obstacle || target.object)
	if is_diggable && target.obstacle:
		var index = get_cellv(target.pos)
		var obj = Resources.create_resource_from_tile_id(index)
		if obj: resource_object_out.append(obj)
	return is_diggable


static func map_from_cell_index_to_cell_type(index):
	match index:
		-2:
			return Enums.CELL_TYPE.ACTOR
		-1:
			return Enums.CELL_TYPE.EMPTY
		0,1:
			return Enums.CELL_TYPE.OBSTACLE
		2:
			return Enums.CELL_TYPE.WALKABLE
		3,4:
			return Enums.CELL_TYPE.OBSTACLE
		5:
			return Enums.CELL_TYPE.OBJECT
		_:
			return Enums.CELL_TYPE.OBSTACLE


static func _debug_request_target(pawn, cell_target_type, is_initial) -> void:
	if Constant.is_verbose():
		var action_phase = "initially" if is_initial else "finally  "
		print("%s, %s, %s targets: %s" %
			[pawn.get_class(), pawn.global_turn_state_counter, action_phase, cell_target_type])