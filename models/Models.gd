"""
Namespace and container for all the model classes.
"""
extends Node

class_name Models

const GridItemResource = preload("GridItem.gd")
const TurnItemResource = preload("TurnItem.gd")

const TurnItem = TurnItemResource.TurnItem
const GridItem = GridItemResource.GridItem
