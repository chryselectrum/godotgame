extends DataModel

class TurnItem:

	var instance_id: int
	var action_name: String
	var action_args: Array
	var action_time: int
	var object_name: String

	func _init(
			instance_id: int,
			action_name: String,
			action_args: Array,
			action_time: int,
			object_name: String):
		self.instance_id = instance_id
		self.action_name = action_name
		self.action_args = action_args
		self.action_time = action_time
		self.object_name = object_name


	static func create_from_pawn(pawn):
		if pawn.action_name.ends_with("_anim"):
			return null
		else:
			return TurnItem.new(
				pawn.get_instance_id(),
				pawn.action_name,
				pawn.action_args,
				pawn.action_time,
				pawn.name)


	static func sort_by_action_time(a, b):
		if a.action_time < b.action_time:
			return true
		return false