extends DataModel

class GridItem:
	var pos: Vector2
	var pawn
	var obstacle
	var object
	var player: Vector2

	func _init(pos, pawn, obstacle, object):
		self.pos = pos
		self.pawn = pawn
		self.obstacle = obstacle
		self.object = object
		self.player = Vector2(-1, -1)
