extends Node

class_name Enums

enum CELL_TYPE { PAWN = -3, ACTOR = -2, EMPTY = -1, WALKABLE = 0, OBSTACLE = 1, OBJECT = 2 }

enum MODE { IDLE, ROAM, HUNT, FLEE, FIGHT }