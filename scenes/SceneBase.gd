"""
Base scene class to set scale of the game.
"""
extends Node2D

func _ready():
	scale.x = Constant.SCALE_X
	scale.y = Constant.SCALE_Y