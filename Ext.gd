"""
\"Extension\" shorthand methods
"""
extends Node

static func node(node: Node, identifier: String):
	return node.get_node(identifier) if is_instance_valid(node.get_node(identifier)) else null


static func get_key(dict: Dictionary, value):
	for k in dict: if dict[k] == value: return k
	return null


static func inverse_dict(dict: Dictionary) -> Dictionary:
	var resultDict: Dictionary = {}
	for key in dict: resultDict[dict[key]] = key
	return resultDict


static func hide(node):
	if node && node != null:
		node.visible = false


static func center(node):
	if node && node != null:
		node.centered = true