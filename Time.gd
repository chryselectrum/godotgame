"""
Time constants
"""
extends Node

const TURN_LENGTH = 6

const BASE_ATTACK_SPEED = 2
const BASE_MOVEMENT_SPEED = 5
const BASE_DIG_SPEED = 66
