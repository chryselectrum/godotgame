extends KinematicBody2D # TODO: Extends Corporeal...

class_name Structure
func get_class() -> String: return "Structure"
func is_class(name: String) -> bool: return name == "Structure" or .is_class(name)

export onready var type
export onready var texture_rect: TextureRect = Ext.node(self, "TextureRect")

func _init():
	type = get_class()


func _ready():
	texture_rect.expand = true
	texture_rect.rect_size.x = Constant.RECT_SIZE
	texture_rect.rect_size.y = Constant.RECT_SIZE
