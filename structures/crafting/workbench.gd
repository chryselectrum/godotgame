extends Structure

class_name Workbench
func get_class() -> String: return "Workbench"
func is_class(name: String) -> bool: return name == "Workbench" or .is_class(name)

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass

