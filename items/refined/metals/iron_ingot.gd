extends Item

class_name IronIngot
func get_class() -> String: return "IronIngot"
func is_class(name: String) -> bool: return name == "IronIngot" or .is_class(name)

