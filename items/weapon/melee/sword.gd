extends Item

class_name Sword
func get_class() -> String: return "Sword"
func is_class(name: String) -> bool: return name == "Sword" or .is_class(name)

