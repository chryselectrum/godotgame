"""
Container listing all the items.
"""
extends Node

# TODO: same as with resources, all references could be constructed reading the file system,
# however, it is somewhat useful to group them into different categories somehow
# ...eg by using the folder structure as a group name

const Slime = preload("Slime.tscn")
const Sword = preload("weapon/melee/Sword.tscn")


const melee_weapons: Array = [Sword]

