"""
Global constants, debug flags etc.
"""
extends Node

const INT_MAX = 9223372036854775807
const FORCE_RELEASE = false

const RECT_SIZE = 20

const SCALE_X = 1.75
const SCALE_Y = 1.75
const BASE_WIDTH = 1024
const BASE_HEIGHT = 768
#const BASE_WIDTH = 1920
#const BASE_HEIGHT = 1080

static func is_debug():
	var is_debug_build = OS.is_debug_build()
	return is_debug_build && !FORCE_RELEASE


static func is_verbose():
	return false


static func remove_debug_state():
	return true


static func remove_debug_paths():
	return true


static func remove_debug_actions():
	return false


static func remove_debug_targets_output():
	return true