extends Pawn

class_name Actor
func get_class() -> String: return "Actor"
func is_class(name: String) -> bool: return name == "Actor" or .is_class(name)

signal player_acted
signal player_acted_finished
signal player_ready

var continue_time = false
var process_line_of_sight = false
var line_of_sight_mutex: Mutex = Mutex.new()

func _ready():
	can_move = true
	movement_speed = Time.BASE_MOVEMENT_SPEED
	attack_speed = Time.BASE_ATTACK_SPEED
	dig_speed = 11

	TurnState.register_player(self)
	connect("player_acted", TurnState, "_on_player_acted")
	connect("player_acted_finished", TurnState, "_on_player_acted_finished")


#func _unhandled_key_input(event: InputEventKey) -> void:
#	var input_direction = get_input_direction(event)
func _process(delta: float) -> void:
	var input_direction = _get_input_direction()

	if input_direction == null && !continue_time:
		return

	if Constant.is_debug():
		grid.nav.clear_lines()

	var target = null

	if action_name == "":
		var dig_object_out = []
		var current_position = self.position
		action_direction = input_direction
		target = grid.request_target(self, input_direction)
		if target:
			target.player = self.position

		if (grid.is_target_movable(target)):
			set_action("move", [target.pos], movement_speed)
		elif grid.is_target_pawn(target):
			set_action("attack", [target], attack_speed)
		elif grid.is_target_diggable(target, dig_object_out):
			var local_dig_speed = dig_speed + dig_object_out[0].dig_speed_mod if dig_object_out.size() > 0 else dig_speed
			set_action("dig", [target], local_dig_speed)
		else:
			set_action("bump", [])

	emit_signal("player_acted", target)

	if Constant.is_debug():
		grid.nav.draw_lines()

	yield(get_tree(), "idle_frame")

	emit_signal("player_acted_finished")


func _physics_process(delta: float) -> void:
	line_of_sight_mutex.lock()
	if !process_line_of_sight:
		line_of_sight_mutex.unlock()
		return

	var visibility_map: TileMap = grid.get_node("../OcclusionGrid")
	var player_tile = position
	var player_center = _tile_to_pixel_center(player_tile.x, player_tile.y)
	var space_state = get_world_2d().direct_space_state
	for x in range(grid.x):
		for y in range(grid.y):
			if visibility_map.get_cell(x, y) == 0:
				var x_dir = 1 if x < player_tile.x else -1
				var y_dir = 1 if y < player_tile.y else -1
				var test_point = _tile_to_pixel_center(x, y) + Vector2(x_dir, y_dir) * Constant.RECT_SIZE / 2

				var occlusion = space_state.intersect_ray(player_center, test_point)
				if !occlusion || (occlusion.position - test_point).length() < 1:
					visibility_map.set_cell(x, y, -1)

	process_line_of_sight = false
	line_of_sight_mutex.unlock()


func _tile_to_pixel_center(x, y):
	return Vector2((x + 0.5) * Constant.RECT_SIZE, (y + 0.5) * Constant.RECT_SIZE)

func _get_input_direction():
	var v = null
	if is_action_pressed("move_up"):
		v = Vector2(0,-1)
	elif is_action_pressed("move_down"):
		v = Vector2(0,1)
	elif is_action_pressed("move_left"):
		v = Vector2(-1,0)
	elif is_action_pressed("move_right"):
		v = Vector2(1,0)
	elif is_action_pressed("move_northeast"):
		v = Vector2(1,-1)
	elif is_action_pressed("move_southeast"):
		v = Vector2(1,1)
	elif is_action_pressed("move_southwest"):
		v = Vector2(-1,1)
	elif is_action_pressed("move_northwest"):
		v = Vector2(-1,-1)
	elif is_action_pressed("idle"):
		v = Vector2(0,0)

	return v


func get_input_direction(event):
	var v = null
	if event.is_action_pressed("move_up"):
		v = Vector2(0,-1)
	elif event.is_action_pressed("move_down"):
		v = Vector2(0,1)
	elif event.is_action_pressed("move_left"):
		v = Vector2(-1,0)
	elif event.is_action_pressed("move_right"):
		v = Vector2(1,0)
	elif event.is_action_pressed("move_northeast"):
		v = Vector2(1,-1)
	elif event.is_action_pressed("move_southeast"):
		v = Vector2(1,1)
	elif event.is_action_pressed("move_southwest"):
		v = Vector2(-1,1)
	elif event.is_action_pressed("move_northwest"):
		v = Vector2(-1,-1)
	elif event.is_action_pressed("idle"):
		v = Vector2(0,0)

	return v


func is_action_pressed(action_key):
	return Key.is_pressed(action_key)


func update_look_direction(direction):
	$Pivot/Sprite.rotation = direction.angle()


func dig(target_position):
	attack(target_position)


func dig_anim(target_position):
	attack_anim()


func idle():
	pass