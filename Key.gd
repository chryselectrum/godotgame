"""
Static shorthands for checking input keys.
"""
extends Node


static func is_pressed(action_key):
	return Input.is_action_pressed(action_key)


static func is_released(action_key):
	return Input.is_action_just_released(action_key)