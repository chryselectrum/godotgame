extends ItemList

onready var parent: Control = get_parent()

const menu_items = []

func _ready():
	max_columns = int(parent.get_rect().size.x / Constant.RECT_SIZE)
	fixed_column_width = Constant.RECT_SIZE - 4

	for weapon in Items.melee_weapons:
		_add_item(weapon.instance())

	if Constant.is_verbose():
		print("Added items count: %s" % get_item_count())

	var err_activate = connect("item_activated", CraftingState, "_on_item_activated", [menu_items])
	var err_selected = connect("item_selected", CraftingState, "_on_item_selected", [menu_items])


func _add_item(item):
	add_icon_item(item.texture)
	menu_items.append(item)
