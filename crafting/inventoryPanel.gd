extends MarginContainer

func _ready() -> void:
	var left = (-Constant.BASE_WIDTH / (Constant.SCALE_X * 2)) - 2
	var top = (-Constant.BASE_HEIGHT /  (Constant.SCALE_Y * 2)) - 10
	margin_left = left
	margin_top = top
