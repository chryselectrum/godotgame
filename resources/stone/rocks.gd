extends ResourceData

class_name Rocks
func get_class() -> String: return "Rocks"
func is_class(name: String) -> bool: return name == "Rocks" or .is_class(name)

func _init() -> void:
	dig_speed_mod = 4