"""
Factory for listing and creating all the resource references.
"""
extends Node

enum RESOURCE { ROCKS }

enum TILE { ROCKS = 3 }

export var tile_id_to_type_dict: Dictionary = {}
export var resources_dict: Dictionary = {}
export var resource_groups_dict: Dictionary = {}

func _init():
	_load_resources("res://resources")
	tile_id_to_type_dict = Ext.inverse_dict(self.TILE)


func create_resource(unique_resource_name: String) -> ResourceData:
	var resource_name = unique_resource_name.to_lower()
	return load(resources_dict[resource_name]).new()


func create_resource_from_tile_id(tile_id: int) -> ResourceData:
	if !tile_id in tile_id_to_type_dict: return null
	return create_resource(tile_id_to_type_dict[tile_id])


func _load_resources(rootFolder):
	var resources = FileSystem.load_resources(rootFolder)
	resources_dict = resources[0]
	resource_groups_dict = resources[1]