"""
Static file system operations.
"""
extends Node


"""
Load all resource files from a file system folder with folders as groups and filenames as keys.
"""
static func load_resources(rootFolder: String) -> Array:
	var resources_dict = {}
	var resource_groups_dict = {}

	var filesDirectories = _get_dir_contents(rootFolder)
	var filepaths = filesDirectories[0]
	var directories = filesDirectories[1]

	for d in directories:
		var group_name = d.replace(rootFolder, "").lstrip("/")
		resource_groups_dict[group_name] = {}

	for f in filepaths:
		var paths = f.rsplit("/", true, 1)
		var file_name: String = paths[1].replace(".gd", "")
		var file_group = paths[0].replace(rootFolder, "").lstrip("/")
		if file_group == "": continue
		resources_dict[file_name] = f
		resource_groups_dict[file_group] = f

	return [resources_dict, resource_groups_dict]


"""
Return all filenames (full filepaths) and directories under root path.
"""
static func _get_dir_contents(rootPath: String) -> Array:
	var files = []
	var directories = []
	var dir = Directory.new()
	if dir.open(rootPath) == OK:
		dir.list_dir_begin(true, false)
		_add_dir_contents(dir, files, directories)
	else:
		push_error("An error occurred when trying to access the path.")
	return [files, directories]


"""
Recursively list and add all filenames and directoreis with full path to their respective arrays.
"""
static func _add_dir_contents(dir: Directory, files: Array, directories: Array) -> void:
	var file_name = dir.get_next()

	while (file_name != ""):
		var path = dir.get_current_dir() + "/" + file_name

		if dir.current_is_dir():
			var subDir = Directory.new()
			subDir.open(path)
			subDir.list_dir_begin(true, false)
			directories.append(path)
			_add_dir_contents(subDir, files, directories)
		else:
			files.append(path)

		file_name = dir.get_next()

	dir.list_dir_end()
